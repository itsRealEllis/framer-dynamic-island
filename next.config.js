const withPWA = require("next-pwa")({
  dest: "public",
  register: true,
  skipWaiting: true,
  disable: process.env.NODE_ENV === "development",
});

const withSvgr = require("next-svgr");

const isProd = process.env.NODE_ENV === "production";

module.exports = withPWA(
  withSvgr({
    reactStrictMode: true,
    output: "export",
    images: {
      remotePatterns: [
        {
          protocol: "https",
          hostname: "upload.wikimedia.org",
        },
      ],
      unoptimized: true, // Changed '=' to ':'
    },
    assetPrefix: isProd ? "/framer-dynamic-island/" : "",
    trailingSlash: true,
  })
);
