import { motion } from "framer-motion";
import { FaPhoneAlt } from "react-icons/fa";
import Image from "next/image";
import { useState } from "react";

const Navbar: React.FC = () => {
  const [hovered, setHovered] = useState(false);

  return (
    <>
      <motion.div
        className="bg-black rounded-full mx-auto flex items-center text-white text-2xl text-center align-middle"
        initial={{ scale: 1 }}
        whileHover={{
          height: 70,
          width: "95%",
          borderRadius: ["999px"],
        }}
        style={{
          height: 40, // Set height when not hovered
          width: 130, // Set width when not hovered
        }}
        onHoverStart={() => {
          setHovered(true);
        }}
        onHoverEnd={() => setHovered(false)}
      >
        {hovered && (
          <>
            <Image
            src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/8e/Visit_of_Tim_Cook_to_the_European_Commission_-_P061904-946789.jpg/800px-Visit_of_Tim_Cook_to_the_European_Commission_-_P061904-946789.jpg"
              className="h-12 ml-3 rounded-full w-12 object-cover"
             alt="Tim Cook"
             height={48}
             width={48}
            />
            <div className="flex items-start flex-col m-0 p-0 ml-[10px]">
              <motion.p
                className="text-sm text-[#565656] font-medium m-0 p-0"
                initial={{ scale: 0 }}
                animate={{
                  scale: 1,
                  transition: {
                    delay: 0.06,
                  },
                }}
              >
                iPhone
              </motion.p>
              <motion.p
                className="text-sm text-white font-medium m-0 p-0 mt-[-4px]"
                initial={{ scale: 0 }}
                animate={{
                  scale: 1,
                  transition: {
                    delay: 0.06,
                  },
                }}
              >
                Tim Cook
              </motion.p>
            </div>
            <div className="flex items-end space-x-2 ml-auto flex-row m-0 p-0 pr-3">
              <motion.div
                className="h-10 bg-[#fe453b] text-base inline-flex justify-center items-center rounded-full w-10"
                initial={{ scale: 0 }}
                animate={{
                  scale: 1,
                  transition: {
                    delay: 0.06,
                  },
                }}
              >
                <FaPhoneAlt className="rotate-[135deg]" />
              </motion.div>
              <motion.div
                className="h-10 bg-[#35d159] text-base inline-flex justify-center items-center rounded-full w-10"
                initial={{ scale: 0 }}
                animate={{
                  scale: 1,
                  transition: {
                    delay: 0.06,
                  },
                }}
              >
                <FaPhoneAlt />
              </motion.div>
            </div>
          </>
        )}
      </motion.div>
    </>
  );
};

export default Navbar;
