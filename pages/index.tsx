import type { NextPage } from "next";
import Head from "next/head";
import Navbar from "../components/navbar";

const Home: NextPage = () => {
  return (
    <div className="w-screen max-w-sm max-h-[800px] mx-auto h-screen">
      <Head>
        <title>Dynamic Island built with Framer</title>
        <link rel="icon" type="image/png" href="https://upload.wikimedia.org/wikipedia/commons/3/33/Apple_logo_arcoiris.png" />
      </Head>

      <main className="w-full h-full bg-slate-500 pt-2">
        <Navbar />
         </main>
    </div>
  );
};

export default Home;
