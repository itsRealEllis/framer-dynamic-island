import { useEffect } from "react";
import "../styles/globals.css";
import type { AppProps } from "next/app";

import { Analytics } from "@vercel/analytics/react";
import { SpeedInsights } from "@vercel/speed-insights/next";

function MyApp({ Component, pageProps }: AppProps) {

  useEffect(() => {
    if (
      typeof window !== "undefined" &&
      navigator.doNotTrack === "1" &&
      !document.cookie
        .split(";")
        .some((cookie) => cookie.trim().startsWith("ANALYTICS=false"))
    ) {
      document.cookie = "ANALYTICS=false; max-age=31536000";
      console.log("ANALYTICS cookie set to false due to 'do not track' request.");
    }
  }, []);
  return (
    <>
      <Component {...pageProps} />
      {process.env.NODE_ENV === "production" &&
      typeof window !== "undefined" &&
      !document.cookie
        .split(";")
        .some((cookie) => cookie.trim().startsWith("ANALYTICS=false")) ? (
        <>
          <Analytics />
          <SpeedInsights />
        </>
      ) : null}
    </>
  );
}

export default MyApp;