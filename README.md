# Next.js + TailwindCSS + Next Sitemap + Next PWA
## Quick deploy
[<img src="https://binbashbanana.github.io/deploy-buttons/buttons/remade/vercel.svg">](https://vercel.com/new/clone?repository-url=https://github.com/TheFrontlineGen/NextJS--Tailwind--Next-Sitemap--Next-PWA-boilerplate)

## Useful Links
Next.js Documentation: https://nextjs.org/docs<br>
TailwindCSS Documentation: https://tailwindcss.com/docs/utility-first<br>
Next Sitemap Documentation: https://next-sitemap.iamvishnusankar.com/docs<br>
Next PWA Documentation: https://github.com/shadowwalker/next-pwa/blob/master/README.md

## Setup instructions
### Install dependencies
**pnpm**
```
pnpm i
```
**npm**
```
npm i
```
**yarn**
```
yarn install
```
### Run development server  
**pnpm**
```
pnpm run dev
```
**npm**
```
npm run dev
```
**yarn**
```
yarn run dev
```

### Setup PWA (optional)
#### "A progressive web app (PWA) is an app that's built using web platform technologies, but that provides a user experience like that of a platform-specific app. Like a website, a PWA can run on multiple platforms and devices from a single codebase." - [Mozilla](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps)

Using a Favicon Generator website [(recommended)](https://favicomatic.com/), create a favicon package with the setting "Every damn size, sir!", making sure to set your app name, URL and colour. Then follow the on screen instructions on how to add the favicon and manifest to your project.
